package com.alighthub.controlPage;

import javax.persistence.Entity;

@Entity
public class News 
{
	
	private int newsId;
	private String newsHeading;
	private String newsDescription;
	
	
	public int getNewsId() {
		return newsId;
	}
	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
	public String getNewsHeading() {
		return newsHeading;
	}
	public void setNewsHeading(String newsHeading) {
		this.newsHeading = newsHeading;
	}
	public String getNewsDescription() {
		return newsDescription;
	}
	public void setNewsDescription(String newsDescription) {
		this.newsDescription = newsDescription;
	}
	
	
	
	

}
